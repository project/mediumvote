MediumVote is based on Eaton's excellent SimpleVote module.

In learning SimpleVote and VotingAPI I wanted to extend some of the features provided by SimpleVote, so I created MediumVote to:

1) Add ajax functionality using drupal.js included in the 4.7 releases.
2) Add multi-criteria voting (on either nodes or comments)
3) A menu option to select voting mode (percentages vs scores)

4) Node-specific voting criteria (newly added)

MediumVote has only been tested on version 4.7, as drupal.js is required.
Also, like SimpleVote, it requires the VotingAPI module, and won't run without it.
It also requires a CSS-capable browser, or the stars voting widget (used when percentage voting option is selectd) won't display properly.

I suggest you familiarize yourself with SimpleVote, VotingAPI and the ajax Handbook tutorials, as the work on MediumVote is largely lifted from that excellent work.

